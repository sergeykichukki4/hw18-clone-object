"use strict";

const testObj = {a: 1, b: [2, 3, {f: 5, g: {n: 6, k: [7, [8,9]]}}], e: 4,};

function customCloneObject(inputObj) {
    let returnObject;

    if (inputObj === null || typeof inputObj !== "object") {
        return inputObj;
    }

    returnObject = Array.isArray(inputObj) ? [] : {};

    for (let key in inputObj) {
        let value = inputObj[key];

        returnObject[key] = customCloneObject(value);
    }

    return returnObject;
}

let user = customCloneObject(testObj);
console.log(user);
testObj["a"]= 4;
console.log(testObj);
console.log(user);
